using GitlabKubernetesBridge.Migrations;
using IdentityModel.Client;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.IdentityModel.Tokens;
using Novell.Directory.Ldap;
using System.Diagnostics;
using System.Security.Claims;
using System.Text.RegularExpressions;

var builder = WebApplication.CreateBuilder(args);

var services = builder.Services;

// Add services to the container.

services.AddControllersWithViews();

//Better Logging in Kubernetes
services.Configure<ForwardedHeadersOptions>(options =>
{
    options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
    options.KnownNetworks.Clear();
    options.KnownProxies.Clear();
});

services.Configure<CookiePolicyOptions>(options =>
{
    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
    options.CheckConsentNeeded = context => true;
    options.MinimumSameSitePolicy = SameSiteMode.None;
});

// Add Cookie settings
services.AddAuthentication(options =>
{
    options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
}).AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
{
    //Refresh Gitlab Access Token automatically
    //see https://stackoverflow.com/a/60895345
    options.Events = new CookieAuthenticationEvents
    {
        // After the auth cookie has been validated, this event is called.
        // In it we see if the access token is close to expiring.  If it is
        // then we use the refresh token to get a new access token and save them.
        // If the refresh token does not work for some reason then we redirect to 
        // the login screen.
        OnValidatePrincipal = async cookieCtx =>
        {
            try
            {
                var now = DateTimeOffset.UtcNow;
                var expiresAt = cookieCtx.Properties.GetTokenValue("expires_at");
                var accessTokenExpiration = DateTimeOffset.Parse(expiresAt);
                var timeRemaining = accessTokenExpiration.Subtract(now);
                // TODO: Get this from configuration with a fall back value.
                var refreshThresholdMinutes = 5;
                var refreshThreshold = TimeSpan.FromMinutes(refreshThresholdMinutes);

                if (timeRemaining < refreshThreshold)
                {
                    var refreshToken = cookieCtx.Properties.GetTokenValue("refresh_token");
                    // TODO: Get this HttpClient from a factory
                    var response = await new HttpClient().RequestRefreshTokenAsync(new RefreshTokenRequest
                    {
                        Address = Environment.GetEnvironmentVariable("GITLAB_URL") + "oauth/token",
                        ClientId = Environment.GetEnvironmentVariable("GITLAB_CLIENT_ID"),
                        ClientSecret = Environment.GetEnvironmentVariable("GITLAB_CLIENT_SECRET"),
                        RefreshToken = refreshToken
                    });

                    if (!response.IsError)
                    {
                        var expiresInSeconds = response.ExpiresIn;
                        var updatedExpiresAt = DateTimeOffset.UtcNow.AddSeconds(expiresInSeconds);
                        cookieCtx.Properties.UpdateTokenValue("expires_at", updatedExpiresAt.ToString());
                        cookieCtx.Properties.UpdateTokenValue("access_token", response.AccessToken);
                        cookieCtx.Properties.UpdateTokenValue("refresh_token", response.RefreshToken);

                        // Indicate to the cookie middleware that the cookie should be remade (since we have updated it)
                        cookieCtx.ShouldRenew = true;
                    }
                    else
                    {
                        cookieCtx.RejectPrincipal();
                        await cookieCtx.HttpContext.SignOutAsync();
                    }
                }
            }
            catch (global::System.Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    };
})
  .AddOpenIdConnect(OpenIdConnectDefaults.AuthenticationScheme, options =>
  {
      options.Authority = Environment.GetEnvironmentVariable("GITLAB_URL"); //with / at the end

      options.ClientId = Environment.GetEnvironmentVariable("GITLAB_CLIENT_ID");
      options.ClientSecret = Environment.GetEnvironmentVariable("GITLAB_CLIENT_SECRET");

      options.Scope.Clear();
      options.Scope.Add("api");
      options.Scope.Add("openid");

      options.SaveTokens = true;

      options.GetClaimsFromUserInfoEndpoint = true;

      options.ResponseType = "code";

      options.CallbackPath = new PathString("/oauth/signin");

      options.ClaimsIssuer = Environment.GetEnvironmentVariable("GITLAB_URL").Substring(0, Environment.GetEnvironmentVariable("GITLAB_URL").Length - 1);

      options.TokenValidationParameters = new TokenValidationParameters
      {
          NameClaimType = "nickname"
      };

      options.Events.OnUserInformationReceived += (UserInformationReceivedContext data) =>
      {
          try
          {
              var username = data.User.RootElement.GetProperty("nickname").GetString() ?? null;

              if (username == null)
              {
                  data.Fail("Wrong information from openid user data endpoint received");
                  return Task.CompletedTask;
              }

              if (!CheckLDAPUser(username))
              {
                  data.Fail($"User {username} not allowed in ldap filter.");
                  data.Response.Redirect("/login?error=401");
                  data.HandleResponse();
              }
              else
              {
                  ClaimsIdentity claimsId = data.Principal.Identity as ClaimsIdentity;
                  foreach (var item in data.User.RootElement.EnumerateObject())
                  {
                      if (item.Value.ValueKind == System.Text.Json.JsonValueKind.String)
                      {
                          claimsId.AddClaim(new Claim(item.Name, item.Value.GetString()));
                      }
                  }
              }
          }
          catch (global::System.Exception ex)
          {
              Console.WriteLine(ex.Message);
              Console.WriteLine(data.User.RootElement.GetRawText());
              Console.WriteLine(data.User.RootElement.GetProperty("nickname"));
              data.Fail("Wrong User info format.");
              data.Response.Redirect("/login?error=401");
              data.HandleResponse();
          }

          return Task.CompletedTask;
      };
  });

//Add OpenAPI Docs
services.AddSwaggerGen();


//App Services
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    //Set Branding
    if (Environment.GetEnvironmentVariable("BRAND_STRING") != null)
    {
        foreach (var item in Directory.GetFiles("wwwroot", "index.html", SearchOption.AllDirectories))
        {
            string indexHTML = File.ReadAllText(item);
            if (!indexHTML.Contains("id='branding'"))
            {
                indexHTML = indexHTML.Replace("</body>", $"<div id='branding' class='d-none'>{Environment.GetEnvironmentVariable("BRAND_STRING")}</div></body>");
                File.WriteAllText(item, indexHTML);
            }
        }
    }
}

app.UseForwardedHeaders();
app.UseCookiePolicy();
app.UseAuthentication();


app.UseDefaultFiles();
app.UseStaticFiles();
app.UseRouting();
app.UseAuthorization();

app.UseSwagger();
app.UseSwaggerUI();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

// In production, the Angular files will be served from this directory/file
app.MapFallbackToFile("index.html");

//Apply v1 to v2 Migration
using var loggerFactory = LoggerFactory.Create(loggingBuilder => loggingBuilder
    .SetMinimumLevel(LogLevel.Trace)
    .AddConsole());
Migrate_from_v1_to_GKA.Migrate(loggerFactory.CreateLogger<Migrate_from_v1_to_GKA>());

using (var p = new Process())
{
    p.StartInfo.FileName = Environment.GetEnvironmentVariable("HELM_PATH") ?? "/usr/local/bin/helm";
    p.StartInfo.ArgumentList.Add("repo");
    p.StartInfo.ArgumentList.Add("update");
    p.Start();
    p.WaitForExit();
    if (p.ExitCode != 0)
    {
        loggerFactory.CreateLogger<Program>().LogCritical("Failed to update Helm repositories. Exiting now. For errors see above.");
        return;
    }
}


app.Run();


/// <summary>
/// Checks wheter the user is allowed to connect (checking with ldap filter string)
/// </summary>
/// <param name="username"></param>
/// <returns></returns>
bool CheckLDAPUser(string username)
{
    if (Environment.GetEnvironmentVariable("LDAP_DISABLED") != null && Environment.GetEnvironmentVariable("LDAP_DISABLED") == "true")
    {
        return true;
    }

    //ldap injection schutz
    username = Regex.Match(username, @"[A-Za-z0-9]+").Value;



    using (var connection = new LdapConnection())
    {
        connection.SecureSocketLayer = Environment.GetEnvironmentVariable("LDAP_USE_SSL") == "true";
        connection.Connect(Environment.GetEnvironmentVariable("LDAP_SERVER_FQDN"), int.Parse(Environment.GetEnvironmentVariable("LDAP_SERVER_PORT")));
        if (connection.Connected)
        {
            connection.Bind(Environment.GetEnvironmentVariable("LDAP_SEARCH_USER"), Environment.GetEnvironmentVariable("LDAP_SEARCH_PASSWORD"));
            if (connection.Bound)
            {
                var res = connection.Search(Environment.GetEnvironmentVariable("LDAP_SEARCH_BASE"), LdapConnection.ScopeSub, Environment.GetEnvironmentVariable("LDAP_FILTER").Replace("%uid%", username), null, false);
                if (res.HasMore())
                {
                    return true;
                }
            }
        }
    }

    return false;
}