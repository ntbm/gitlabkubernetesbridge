![Logo](./images/logo.png)
# Gitlab Kubernetes Bridge (GKB)
GKB is a plattfom to link Gitlab porjects with Kubernetes namespaces by one click.
You also can give your users a way to create their own Kubernetes namespace to deploy their applications.
GKB comes with a Web UI providing One-click provioning which deploys all needed things like creating a Gitlab project unique Kubernetes namespace, deploying a Gitlab Kubernetes Agent, setting up the Gitlab project with all needed CI/CD variables for Auto DevOps.
This Web UI also includes a Kubernetes Dashboard to let the users view their own Kubernetes namespaces.
GKB integrates with Gitlab SSO provider therefore configuration is as easy as it should be.
GKB also let you restrict the users able to use it by specifing a LDAP user filter.
GKB will match the by SSO retrieved user name against your LDAP filter if you use this feature.
This provides additional security in multi-user environments.
  
### Functions
* create Kubernetes namespaces for Gitlab projects
* link a private runner to these projects
* auto link the gernerated Kubernetes namespace to a user specific Kubernetes serviceaccount
* kubectl configuration file generator for user serviceaccounts
* kubernetes dashboard integration

![GUI snapshot](./images/ui.png)

## Installation
You need to have Helm (v3) installed. (see https://helm.sh)  
1. Create an GitLab Application (in your user settings) and add the scopes `openid` and `api`. The callback will be `<public_url>/oauth/signin`. Whre `public_url` is the url where your ingress will serve the app later.
2. Create (or modify first) the applied user roles by GKB by running:
```bash
git clone https://gitlab.com/MBcom/gitlabkubernetesbridge.git
cd gitlabkubernetesbridge
kubectl apply -f user-roles.yml


helm repo add gitlabkubernetesbridge https://gitlab.com/api/v4/projects/14954793/packages/helm/stable
```   

* **mlu-user-namespaces-role**: is applied namespace bound to the user service account
* **gitlab-namespaces-role**: is applied to the default service account in the project namespaces, which is also the service account used in the gitlab kubernetes integration
* **mlu-user-global-role**: is applied as clusterrole binding on user service account creation, currently it allows to list all namespace
3. Run the following command on a machine with kubernetes access : (replace the values in `<>` first, hint: you can also configure branding and/or disable LDAP user restriction - see Charts/values.yaml)
```bash
helm upgrade --install \
        --wait \
        --set service.url="<public_url>" \
        --set kubernetes-dashboard.ingress.hosts[0]="<public_url>" \
        --set 'kubernetes-dashboard.ingress.annotations."nginx.ingress.kubernetes.io/auth-url"=https://<public_url>/api/v1/kubernetes/oauth/auth'
        --set 'kubernetes-dashboard.ingress.annotations."nginx.ingress.kubernetes.io/auth-signin"=https://<public_url>/login?redirect=https://$host$request_uri$is_args$args'
        --set image.pullPolicy=Always \
        --set master.LDAP_DISABLED="false" \
        --set master.LDAP_SERVER_FQDN="<ldapServerFQDN>" \
        --set master.LDAP_SEARCH_USER="<ldapUserName>" \ # a ldap user for filter lookups
        --set master.LDAP_SEARCH_PASSWORD="<ldapUserPassword>" \ # the password for above user
        --set master.LDAP_FILTER="<ldapFilter>" \
        --set master.ENCRYPTION_KEY="<ENCRYPTION_KEY>" \ # a random key for user data encryption - e.g. generate one with openssl rand -hex 32
        --set master.LDAP_SEARCH_BASE="<ldapSearchBase>" \ 
        --set master.GITLAB_URL="https://gitlab.example.com/" \ # the url of your gitlab instance with a `/` at the end
        --set master.GITLAB_CLIENT_SECRET="<GITLAB_CLIENT_SECRET>" \ # the client secret generated in step 1
        --set master.GITLAB_CLIENT_ID="<GITLAB_CLIENT_ID>" \ # the client id generated in step 1
        --set master.SPECIAL_RUNNER_ID="<SPECIAL_RUNNER_ID>" \ # id of the privileged runner
        --set master.KUBE_APIURL="<KUBE_APIURL>" \
	    --set master.GITLAB_KUBERNETES_AGENT_ENDPOINT="wss://kas.gitlab.example.com" \ # the KAS Endpoint of your Gitlab Instance
        --namespace="<KUBE_NAMESPACE>" \
        "<name>" \
        gitlabkubernetesbridge/gkb

```

# Link one Gitlab project to multiple clusters
GKB can link a Gitlab project to exactly one Kubernetes Namespace in a single Cluster.
To achieve an 1:n link for a Gitlab Project you can deploy in each Cluster a GKB instance.
You can also deploy in a single cluster multiple GKB instances.
To ensure that they do not conflict each other you have to change `GITLAB_KUBE_CONTEXT_NAME`, `NAMESPACE_PROJECT_MEMBER_LABEL_NAME`, `NAMESPACE_PROJECT_OWNER_LABEL_NAME`, `NAMESPACE_PROJECT_LABEL_NAME` at minimum.
If you deploy multiple instances to the same Cluster you also have to set `KUBE_NAMESPACE_NAME_POSTFIX` to a alphanumeric string to distingush the created namespaces.

# Automatic Update of Gitlab Kubernetes Agents in the namespaces managed by this GKB
        
Set `HELM_GITLAB_KUBERNETES_AGENT_CHART_VERSION` to:
* A specific version of gitlab/gitlab-agent to specify a chart version that should be installed/ upgraded to 
  (list all version with helm search repo gitlab/gitlab-agent -l 
  (You have to add once the Gitlab Helm Chart repo before by running: helm repo add gitlab https://charts.gitlab.io)) 
  (This would also downgrade if you specify an older chart version.) 
  To upgrade the Gitlab Kubernetes Agents change the version in your values.yaml and redploy the GKB instance.
  This will create a Kubernetes Job which will upgrade the Gitlab Kubernetes Agents sequentially in all namespaces managed by the GKB instance.
* "latest" to install the lastest version of the chart available by rolling out this chart. You have to rerun the created Kubernetes Job, and restart the GKB Pod to apply these changes.
  We do not recommend to use this option.
* You can also leave this variable undefined to disable a upgrade process through GKB. The installed Gitlab Kubernetes Agent charts version correspondens then with the lastest version available at GKB Pod start time.

# Configure external monitoring URL
GKB allows you to add a Kubernetes namespace specific external Monitoring Link to each Kubernetes Namespace UI entry.
To do so, specify `master.KUBERNETES_NAMESPACE_MONITORING_URL` in your `values.yaml`.
The string `{NAMESPACE_NAME}` will be replaced by the actual namespace name.

## Example: Link Rancher Cluster Monitoring to GKB
You can link a Rancher Cluster Monitoring Deployment.
If you do not use Rancher, you can easily deploy for example in all other Kubernetes distros by using their Helm Chart.
(https://github.com/rancher/charts/tree/main/charts/rancher-monitoring)

You can also use the official Prometheus community chart.
```bash
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo add stable https://charts.helm.sh/stable/
helm repo update
```

We will 'deploy' Grafana under a suburl of GKB in this example.
Doing this has the advantage of being able to use the GKB for authorization.
So, we will need to run Grafana under a suburl which implies the following configuration changes (these changes work well with the Rancher Grafana Proxy Link):

```yaml
grafana:
  enabled: true
  grafana.ini:
    auth:
      disable_login_form: false
    auth.anonymous:
      enabled: true
      org_role: Viewer
    users:
      auto_assign_org_role: Viewer
    server:
      root_url: "%(protocol)s://%(domain)s:%(http_port)s/grafana/"
      serve_from_sub_path: "true"
```

Create the following ingress ressource in the namespace where you have deployed the monitoring with the configuration above:
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/auth-signin: https://<public_url>/login?redirect=https://$host$request_uri$is_args$args
    nginx.ingress.kubernetes.io/auth-url: https://<public_url>/api/v1/kubernetes/oauth/auth
    nginx.ingress.kubernetes.io/rewrite-target: /$1
  name: gkb-monitoring
  namespace: cattle-monitoring-system
spec:
  rules:
  - host: <public_url>
    http:
      paths:
      - backend:
          service:
            name: rancher-monitoring-grafana
            port:
              number: 80
        path: /grafana/(.*)
        pathType: Prefix
```

This configuration ensures a user is authenticated to your GKB instance before being able to access grafana as view only user.
You have to adjust the namespace name, and service name accordingly to your deployment.
The provided values work directly for Rancher Monitoring deployments with the configuration changes provided before.
