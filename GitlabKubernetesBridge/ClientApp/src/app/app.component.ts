import { Component, Inject } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  window = window;
  isNavbarCollapsed = true;

  constructor(@Inject('BRAND_STRING') public brandString: string) {

  }
}
