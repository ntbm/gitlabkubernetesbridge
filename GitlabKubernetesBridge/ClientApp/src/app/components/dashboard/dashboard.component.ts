import { Component, OnInit } from '@angular/core';
import { GitlabService, GitlabProject, KubernetesService, CustomProjectsService, CustomProject } from '../../api';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public projekte?: GitlabProject[];

  public extendedProject: number = -1;

  public extendedCustomProject: string = "";

  public customProjects?: CustomProject[];

  public newNamespaceName: string = "";

  constructor(
    private projekteAPI: GitlabService,
    private kubernetesAPI: KubernetesService,
    private customProjectsAPI: CustomProjectsService
  ) {

  }

  ngOnInit() {
    this.projekteAPI.getGitlabProjects().subscribe(
      value => {
        this.projekte = value;
      },
      error => {
        console.error(error);
      },
      () => {

      }
    );

    this.customProjectsAPI.listCustomProjects().subscribe(
      value => {
        this.customProjects = value;
      },
      error => {
        console.error(error);
      },
      () => {

      }
    );
  }


  CountOfJoinedProjects(): number {
    if (!this.projekte) return 0;
    return this.projekte.reduce<number>((a, b) => a + (b.joined ? 1 : 0), 0);
  }

  GetToken(tooltip: NgbTooltip, btn: HTMLButtonElement): void {
    btn.disabled = true;
    fetch("/api/v1/kubernetes/getUserToken", {
      method: "POST"
    }).then(r => {
      if (r.ok) {
        r.text().then(value => {
          this.copyMessage(value);
          tooltip.ngbTooltip = "Copied to clipboard.";
          tooltip.open();
          btn.disabled = false;
        })
      } else {
        console.error(r);
        tooltip.ngbTooltip = "Error getting token";
        tooltip.open();
        btn.disabled = false;
      }
    })
  }

  GetProjectToken(tooltip: NgbTooltip, pId: number, btn: HTMLButtonElement): void {
    btn.disabled = true;
    this.projekteAPI.getKubernetesToken(pId).subscribe(
      value => {
        this.copyMessage(value.token + "");
        tooltip.ngbTooltip = "Copied to clipboard.";
        tooltip.open();
        btn.disabled = false;
      },
      error => {
        console.error(error);
        tooltip.ngbTooltip = "Error getting token: " + JSON.stringify(error);
        tooltip.open();
        btn.disabled = false;
      },
      () => {

      }
    )
  }

  EnableProject(p: GitlabProject) {
    if (confirm("I have verified that only trusted people/ staff member have more than Developer privileges in this project.")) {
      p.apiOperationRunning = true;
      this.projekteAPI.enableGitlabProject(p.id).subscribe(
        value => {
          p.UserWithAccess = value.UserWithAccess;
          p.KubernetesNamespace = value.KubernetesNamespace;
          p.apiOperationRunning = false;
          p.joined = value.joined;
          p.enabled = value.enabled;
        },
        error => {
          console.error(error);
          alert(JSON.stringify(error));
          p.apiOperationRunning = false;
        },
        () => {

        }
      )
    }
  }

  DisableProject(p: GitlabProject) {
    if (confirm("ATTENTION: This will delete all data in the Kubernetes namespace, including persistent volumes. Do you really want to continue?")) {
      p.apiOperationRunning = true;
      this.projekteAPI.disableGitlabProject(p.id).subscribe(
        value => {
          p.apiOperationRunning = false;
          p.enabled = false;
        },
        error => {
          console.error(error);
          alert(JSON.stringify(error));
          p.apiOperationRunning = false;
        },
        () => {

        }
      )
    }
  }

  RemoveAccessForUser(u: string, p: GitlabProject) {
    p.apiOperationRunning = true;
    this.projekteAPI.removeUserAccessToKubernetesNamespace(p.id, u).subscribe(
      value => {
        p.UserWithAccess = p.UserWithAccess.filter(e => e != u);
        p.apiOperationRunning = false;
      },
      error => {
        console.error(error);
        alert(JSON.stringify(error));
        p.apiOperationRunning = false;
      },
      () => {

      }
    );
  }

  AddUserToCustomProject(p: CustomProject, username: string) {
    this.customProjectsAPI.addUserToCustomProject(p.id, username).subscribe(
      value => {
        p.userWithAccess.push(username);
      },
      error => {
        console.error(error);
        alert(JSON.stringify(error));
      },
      () => { }
    )
  }

  AddCustomNamespace(name: string): void {
    this.customProjectsAPI.addCustomProject(name).subscribe(
      value => {
        this.customProjects?.push(value);
      },
      error => {
        console.error(error);
        alert(JSON.stringify(error));
      },
      () => { }
    )
  }

  LeaveCustomProject(p: CustomProject): void {
    p.apiOperationRunning = true;
    this.customProjectsAPI.leaveCustomProject(p.id).subscribe(
      value => {
        if (this.customProjects) {
          this.customProjects = this.customProjects.filter(pp => pp.id != p.id);
        }
        p.apiOperationRunning = false;
      },
      error => {
        console.error(error);
        alert(JSON.stringify(error));
        p.apiOperationRunning = false;
      },
      () => { }
    )
  }

  DeleteCustomProject(p: CustomProject): void {
    if (confirm("ATTENTION: This will delete all data in the Kubernetes namespace, including persistent volumes. Do you really want to continue?")) {
      p.apiOperationRunning = true;
      this.customProjectsAPI.deleteCustomProject(p.id).subscribe(
        value => {
          if (this.customProjects) {
            this.customProjects = this.customProjects.filter(pp => pp.id != p.id);
          }
          p.apiOperationRunning = false;
        },
        error => {
          console.error(error);
          alert(JSON.stringify(error));
          p.apiOperationRunning = false;
        },
        () => { }
      )
    }
  }

  RemoveAccessForUserToCustomProject(username: string, p: CustomProject): void {
    p.apiOperationRunning = true;
    this.customProjectsAPI.removeUserFromCustomProject(p.id, username).subscribe(
      value => {
        p.userWithAccess = p.userWithAccess.filter(pp => pp != username);
        p.apiOperationRunning = false;
      },
      error => {
        console.error(error);
        alert(JSON.stringify(error));
        p.apiOperationRunning = false;
      },
      () => { }
    )
  }

  private copyMessage(val: string) {
    const selBox = window.document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    window.document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    window.document.execCommand('copy');
    window.document.body.removeChild(selBox);
  }
}
