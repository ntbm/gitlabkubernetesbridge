/*
 * API description for gitlab-kubernetes-bridge
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 */

using System.Text.Json;
using System.Text.Json.Serialization;

namespace GitlabKubernetesBridge.Server.Models.GitlabAPI
{
    /// <summary>
    /// 
    /// </summary>

    public partial class GetKubeCluster
    {


        /// <summary>
        /// Gets or Sets NameWithNamespace
        /// </summary>
        [JsonPropertyName("name")]
        public string name { get; set; }


        /// <summary>
        /// Gets or Sets managed
        /// </summary>
        [JsonPropertyName("id")]
        public int id { get; set; }


        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonSerializer.Serialize(this, new JsonSerializerOptions { WriteIndented = true });
        }
    }
}
